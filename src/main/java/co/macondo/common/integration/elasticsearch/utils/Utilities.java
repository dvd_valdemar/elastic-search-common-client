package co.macondo.common.integration.elasticsearch.utils;

import java.util.UUID;

import org.springframework.stereotype.Component;

@Component
public class Utilities {
	
	public static String getUuid() {		 
		return UUID.randomUUID().toString().replace("-", "");
	}

}
