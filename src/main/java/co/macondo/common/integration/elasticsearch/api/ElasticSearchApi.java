package co.macondo.common.integration.elasticsearch.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import co.macondo.common.integration.elasticsearch.services.ElasticSearchService;
import co.macondo.common.integration.elasticsearch.utils.Utilities;
import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/elastic-search/api")
@Slf4j
public class ElasticSearchApi {
	
	@Autowired
	private ElasticSearchService elasticSearchClient;
	
	@RequestMapping(value = "/insertar/{index}", method = RequestMethod.POST)
	public ResponseEntity<?> create(@PathVariable("index") String  index, @RequestBody String request) {
		return  elasticSearchClient.create(Utilities.getUuid(), index, request);
	}
	
	@RequestMapping(value = "/buscar/{index}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public ResponseEntity<?> searchQuery(@PathVariable("index") String  index, @RequestBody String request) {
		ResponseEntity<?> response =  elasticSearchClient.search(Utilities.getUuid(), index, request);
		log.info("{} - {} ", null,response);
		return response;
	}
	
	@RequestMapping(value = "/actualizar/{index}/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> updateById(@PathVariable("index") String  index,@PathVariable("index") String  id,  @RequestBody String request) {
		  ResponseEntity<?> response = elasticSearchClient.updateById(Utilities.getUuid(), index, id, request);
		  log.info("{} - {} ", null,response);
		  return response;
	}
	
	@RequestMapping(value = "/actualizar/{index}", method = RequestMethod.GET)
	public ResponseEntity<?> updateByQuery(@PathVariable("index") String  index, @RequestBody String request) {
		return  elasticSearchClient.updateByQuery(Utilities.getUuid(), index, request);
	}
	
	@RequestMapping(value = "/eliminar/{index}/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> deleteByQuery(@PathVariable("index") String  index, @RequestBody String request) {
		return  elasticSearchClient.deleteByquery(Utilities.getUuid(), index, request);
	}

}
