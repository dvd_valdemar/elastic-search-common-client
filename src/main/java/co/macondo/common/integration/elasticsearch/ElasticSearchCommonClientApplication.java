package co.macondo.common.integration.elasticsearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;


@EnableFeignClients
@SpringBootApplication
public class ElasticSearchCommonClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(ElasticSearchCommonClientApplication.class, args);
	}

}
