package co.macondo.common.integration.elasticsearch.client;

import feign.Headers;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import co.macondo.common.integration.elasticsearch.config.ClientConfig;

@FeignClient(name = "elasticsearch-service", url = "${elastic.endpoint}", configuration = ClientConfig.class)
@Headers("Accept: application/json")
public interface ElasticSearchClient {

	@PostMapping(value = "{index}${elastic.method.create}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> create(@RequestHeader("Authorization") String header,
			@RequestBody String request, @PathVariable("index") String index);
	
	@GetMapping(value = "{index}${elastic.method.search}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String search(@RequestHeader("Authorization") String header, @RequestBody String request, @PathVariable("index") String index);

	@PostMapping(value = "{index}${elastic.method.update}{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> updateById(@RequestHeader("Authorization") String header,
			@RequestBody String request,  @PathVariable("id") String id, @PathVariable("index") String index);
	
	@PostMapping(value = "{index}${elastic.method.update}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> updateByQuery(@RequestHeader("Authorization") String header,
			@RequestBody String request, @PathVariable("index") String index);
	
	@PostMapping(value = "{index}${elastic.method.delete.byquery}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> deleteByquery(@RequestHeader("Authorization") String header,
			@RequestBody String request, @PathVariable("index") String index);

}
