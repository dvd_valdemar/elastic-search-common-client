package co.macondo.common.integration.elasticsearch.utils;




import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@PropertySource("file:conf/config.properties")
public class PropertiesUtil {

	@Value("${elastic.endpoint}")
	private String elasticEndpoint;
	
	@Value("${elastic.method.search}")
	private String elasticSearch;
	
	@Value("${elastic.method.update}")
	private String elasticUpdate;
	
	@Value("${elastic.method.update.byquery}")
	private String elasticUpdateByQuery;
	
	@Value("${elastic.method.delete.byquery}")
	private String elasticDeleteByQuery;
	
	@Value("${elastic.user}")
	private String elasticUser;
	
	@Value("${elastic.password}")
	private String elasticPass;
			
			
}

