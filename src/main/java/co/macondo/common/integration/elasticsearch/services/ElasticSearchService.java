package co.macondo.common.integration.elasticsearch.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;

import co.macondo.common.integration.elasticsearch.client.ElasticSearchClient;
import co.macondo.common.integration.elasticsearch.utils.Constants;
import co.macondo.common.integration.elasticsearch.utils.PropertiesUtil;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ElasticSearchService {
	
	@Autowired
	private PropertiesUtil propertiesUtil;
	
	@Autowired
	private ElasticSearchClient elasticSearchCliente;
	
	public ResponseEntity<?> create(String uuid, String index, String request) {
		log.info("{} - index:{}, request: {}", uuid,index,request.replace("\r\n", " ").replace("\n", " "));
		return elasticSearchCliente.create(basicAuthHeader(), request, index);		
	}
	
	public ResponseEntity<?> search(String uuid, String index, String request) {
		log.info("{} - index:{}, request: {}", uuid,index,request.replace("\r\n", " ").replace("\n", " "));
		String resp =  elasticSearchCliente.search(basicAuthHeader(), request, index);
		ResponseEntity<?> response = new ResponseEntity<String>(resp, HttpStatus.OK);
		return response ;
	}
	
	public ResponseEntity<?> updateById(String uuid, String index, String id, String request) {
		log.info("{} - index:{}, id:{}, request: {}", uuid,index,id,request.replace("\r\n", " ").replace("\n", " "));
		return elasticSearchCliente.updateById(basicAuthHeader(), request, id, index);		
	}
	
	public ResponseEntity<?> updateByQuery(String uuid, String index, String request) {
		log.info("{} - index:{}, request: {}", uuid,index,request.replace("\r\n", " ").replace("\n", " "));
		return elasticSearchCliente.updateByQuery(basicAuthHeader(), request, index);		
	}
	
	public ResponseEntity<?> deleteByquery(String uuid, String index, String request) {
		log.info("{} - index:{}, request: {}", uuid,index,request.replace("\r\n", " ").replace("\n", " "));
		return elasticSearchCliente.deleteByquery(basicAuthHeader(), request, index);		
	}
	
    private String basicAuthHeader() {

        byte[] encodedBytes = Base64Utils.encode((propertiesUtil.getElasticUser() + ":" + propertiesUtil.getElasticPass()).getBytes());

        String authHeader = Constants.BASIC_PREFIX + new String(encodedBytes);
        
        return authHeader;
    }

}
